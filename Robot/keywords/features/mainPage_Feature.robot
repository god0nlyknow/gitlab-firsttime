*** Settings ***
Library         SeleniumLibrary
Resource        ${CURDIR}/../pages/mainPage.robot


*** Keywords ***
Click Search After Input Text In Text Box
    [Arguments]         ${Input}
    mainPage.Input Text In Search Box   ${Input}
    mainPage.Click Search In Main Page

Input Text In Search Box With For Loop And Click Search
    [Arguments]         ${productThatIWant}      @{Products}         
    Input Text In Search Box With For Loop      ${productThatIWant}      @{Products}                   
    mainPage.Click Search In Main Page
*** Settings ***
Library         SeleniumLibrary
Variables       ${CURDIR}/../../config/config.yaml


*** Keywords ***
Open Makro Web Site
    Open Browser        url=https://www.makroclick.com/th           browser=gc
    Maximize Browser Window

Close Popup In Main Page
    Wait Until Element Is Visible       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'event')]
    Click Element       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'event')]

Input Text In Search Box
    [Arguments]         ${Input}
    Wait Until Element Is Visible   xpath=//div[contains(@class,'search')]//input
    Input Text          xpath=//div[contains(@class,'search')]//input        ${Input}

Click Search In Main Page
    Wait Until Element Is Visible       xpath=//div[contains(@class,'search')]//i
    Click Element       xpath=//div[contains(@class,'search')]//i

Open Makro Web Site With IF Function
    IF          '${config['select_enviroment']}' == 'qa'
        Open Browser        url=${config['URL']['qa']}            browser=gc
        Maximize Browser Window
    ELSE IF     '${config['select_enviroment']}' == 'dev'
        Open Browser        url=${config['URL']['dev']}           browser=gc
        Maximize Browser Window
    ELSE IF     '${config['select_enviroment']}' == 'production'
        Open Browser        url=${config['URL']['production']}    browser=gc
        Maximize Browser Window
    ELSE
        Log To Console      Not Match
    END

Input Text In Search Box With For Loop
    [Arguments]         ${productThatIWant}     @{Products}         
    FOR     ${Product}  IN  @{Products}
        IF    '${Product}' == '${productThatIWant}'
            Wait Until Element Is Visible   xpath=//div[contains(@class,'search')]//input
            Input Text          xpath=//div[contains(@class,'search')]//input        ${Product}
        END
    END
*** Settings ***
Library         SeleniumLibrary



*** Keywords ***
Add Item With + Button
    [Arguments]         ${nameOfProduct}
    Wait Until Element Is Visible   xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]
    Click Element       xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]

List Of Product
    @{elements}    Get WebElements     //div[contains(@class, "ProductCard__TitleMaxLines")]
    FOR    ${element}     IN    @{elements}
        ${textFromScreen}       Get Text        ${element}
        Log To Console  ${textFromScreen}
    END
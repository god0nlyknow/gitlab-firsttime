*** Settings ***
Library         RequestsLibrary
Library         JSONLibrary


*** Keywords ***
Get All users
    [Arguments]         ${path}
    Create Session          alias=getAllUser            url=https://reqres.in/
    ${response}=        Get On Session          getAllUser          ${path}
    [Return]            ${response}

Register user
    [Arguments]         ${email}            ${password}         ${path}         ${expected_status}
    Create Session          alias=registerUser          url=https://reqres.in/
    ${request_body}=            Create Dictionary           email=${email}            password=${password}
    ${response}=            Post On Session         registerUser            ${path}           json=${request_body}            expected_status= ${expected_status}
    [Return]            ${response}

Register unsuccessful
    [Arguments]         ${email}            ${path}         ${expected_status}
    Create Session          alias=registerUnsuccess         url=https://reqres.in/
    ${request_body}=        Create Dictionary           email=${email}
    ${response}=            Post On Session         registerUnsuccess            ${path}           json=${request_body}            expected_status=${expected_status}
    [Return]            ${response}

Update user
    [Arguments]         ${name}         ${job}          ${path}         ${expected_status}
    Create Session          alias=updateUser            url=https://reqres.in/
    ${request_body}=        Create Dictionary           name=${name}          job=${job}
    ${response}=            Put On Session         updateUser            ${path}           json=${request_body}            expected_status=${expected_status}
    [Return]            ${response}

Delete user
    [Arguments]         ${path}         ${expected_status}
    Create Session          alias=deleteUser         url=https://reqres.in/
    ${response}=            Delete On Session         deleteUser            ${path}            expected_status=${expected_status}
    [Return]            ${response}


Verify Status Should Be Equal
    [Arguments]         ${response}         ${expected_status}
    Should Be Equal as Strings          ${response}         ${expected_status}
    




*** Test Cases ***
TS01 Get All users
    ${response}=    Get All users         /api/users
    Verify Status Should Be Equal       ${response.status_code}     200
    #Log To Console          ${response.json()}

TS02 Register user
    ${response}=    Register user  eve.holt@reqres.in  pistol  /api/register  200
    Verify Status Should Be Equal  ${response.status_code}  200
    Log To Console          ${response.json()}

TS03 Register unsuccessful
    ${response}=    Register unsuccessful  sydney@fife  /api/register   400
    Verify Status Should Be Equal  ${response.status_code}  400
    Log To Console          ${response.json()}

TS04 Update user
    ${response}=    Update user  morpheus  zion resident  /api/users/2  200
    Verify Status Should Be Equal  ${response.status_code}  200
    Log To Console          ${response.json()}

TS05 Delete user
    ${response}=    Delete user  /api/users/2  204
    Verify Status Should Be Equal  ${response.status_code}  204
    Log To Console          ${response.status_code}

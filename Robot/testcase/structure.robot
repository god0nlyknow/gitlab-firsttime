*** Settings ***
Resource        ${CURDIR}/../keywords/pages/mainPage.robot
Resource        ${CURDIR}/../keywords/pages/searchResultPage.robot
Resource        ${CURDIR}/../keywords/features/mainPage_Feature.robot


*** Variables ****
${nameOfProduct}    พีเอสฟู้ดส์ สามชั้นหมูกระทะแช่แข็ง 1000 กรัม
${searchText}       เนื้อหมู


*** Test Cases ***
Assignment
    mainPage.Open Makro Web Site
    mainPage.Close Popup In Main Page
    mainPage_Feature.Click Search After Input Text In Text Box  ${searchText}
    #mainPage.Input Text In Search Box
    #mainPage.Click Search In Main Page
    searchResultPage.Add Item With + Button  ${nameOfProduct}


    #Open Browser        url=https://www.makroclick.com/th           browser=gc
    #Maximize Browser Window
    #Wait Until Element Is Visible       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'event')]
    #Click Element       xpath=//a[contains(@data-dismiss,'modal')]//ancestor::div[contains(@id,'event')]
    #Wait Until Element Is Visible   xpath=//div[contains(@class,'search')]//input
    #Input Text          xpath=//div[contains(@class,'search')]//input        เนื้อหมู
    #Click Element       xpath=//div[contains(@class,'search')]//i
    #Wait Until Element Is Visible   xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]
    #Click Element       xpath=//div[.='${nameOfProduct}']//ancestor::div[contains(@class,'ProductCard')]//button[contains(@id,'plus')]


*** Settings ***
Library         SeleniumLibrary
Resource        ${CURDIR}/../keywords/pages/mainPage.robot
Resource        ${CURDIR}/../keywords/pages/searchResultPage.robot
Resource        ${CURDIR}/../keywords/features/mainPage_Feature.robot
Variables       ${CURDIR}/../config/config.yaml

Suite Teardown  Close Browser


*** Variables ***
@{Products}      ทีวี     ตู้เย็น    ไมโครเวฟ
${productThatIWant}     ตู้เย็น




*** Test Cases ***
Assignment 04 Drive into robot framework
    Open Makro Web Site With IF Function
    Close Popup In Main Page
    Input Text In Search Box With For Loop And Click Search         ${productThatIWant}         @{Products}
    List Of Product
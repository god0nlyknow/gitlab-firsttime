*** Settings ***
Library         SeleniumLibrary
Library         String

*** Keywords ***
Wait And Input
    [Arguments]         ${Input}
    Wait Until Element Is Visible           id=gh-ac
    Input Text          id=gh-ac            ${Input}

Select Dropdown And Click
    [Arguments]         ${Input}
    Select From List By Label   id=gh-cat   ${Input}
    Click Element       id=gh-btn

*** Test Cases ***
TC-01 Test RobotFramework
    Open Browser         url=https://www.ebay.com/            browser=gc
    Wait And Input          Mobile Phone
    Select Dropdown And Click           Cell Phones & Accessories
    ${text from screen}=    Get Text            xpath=//h1[contains(@class,'count')]/span
    ${result}=  Remove String           ${text from screen}         ,
    Should Be True          ${result}>100

    #Log To Console      ${text from screen}
    #Wait Until Element Is Visible   xpath=//div[@class="follow-heart-wrapper heartIcon "]
    #Click Element       xpath=//div[@class="follow-heart-wrapper heartIcon "]